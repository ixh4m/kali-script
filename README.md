# Kali-Script
	Replaces the sources.list file and Adds Repositories of Kali and Debian
	Options to install Discord, Teamviewer, Anydesk, VSCode and Steam

# Usage:
	
	git clone https://gitlab.com/ishamjaglan/kali-script.git
	cd kali-script
	sudo chmod +x kali.sh
	./kali.sh
